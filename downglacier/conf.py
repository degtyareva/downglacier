"""  Configuration file and options

A number of globals (see below) are defined to be available everywhere.

Copyright: Fabien Maussion, 2014-2015

License: GPLv3+
"""
import sys
import os
from configobj import ConfigObj, ConfigObjError
import logging as log
from datetime import datetime
import shutil

# Locals
import downglacier.dgio as dgio

cfgfile = 1 # Config file
cp = 1 # ConfigObj parser
wdir = 1 # Working directory

dfpred = 1 # predictor dataframe
dfinfo = 1 # info variables dataframe
varcont = 1 # variable container object
outercv = 1 # outer crossval ite
user_z = 1 # did the user set zlevels?

def initialize(file=None):

    global cfgfile # Config file
    global cp # ConfigObj parser
    global wdir # Working directory

    global dfpred # predictor dataframe
    global dfinfo # info variables dataframe
    global varcont # variable container object
    global outercv # outer crossval ite
    global user_z # did the user set zlevels?

    cfgfile = file

    try:
        cp = ConfigObj(cfgfile, file_error=True)
    except (ConfigObjError, IOError) as e:
        print('File could not be parsed ({}): {}'.format(cfgfile,e))
        print('Exit program now')
        sys.exit()

    wdir = cp['I/O']['working_dir']
    if not os.path.exists(wdir):
        os.makedirs(wdir)

    log.basicConfig(format='%(asctime)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=log.INFO)

    log.info('!!! ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S") +
                 ' DownGlacier Main program !!!')

    log.info('# Configfile: ' + cfgfile)

    # Copy Configfile to working directory for documentation purposes
    shutil.copyfile(cfgfile, os.path.join(wdir, 'config.cfg'))

    # Read the variable keys
    vkeys = dict((v.strip().lower(), k.strip().lower())
                 for k in cp['Vkeys'] for v in cp['Vkeys'].as_list(k))
    # Read the info keys
    ikeys = dict((v.strip().lower(), k.strip().lower())
                 for k in cp['Ikeys'] for v in cp['Ikeys'].as_list(k))

    # Did the user have specific requirements?
    if 'ignore_predictands' in cp['I/O']:
        s = cp['I/O'].as_list('ignore_predictands')
        for vn in s:
            del vkeys[vn.strip().lower()]
    if 'keep_predictands' in cp['I/O']:
        s = cp['I/O'].as_list('keep_predictands')
        s = [z.strip().lower() for z in s]
        vkeys = {z: vkeys[z] for z in s}

    log.info('# Parsed variable keys: {0}'.format(list(vkeys.keys())))
    log.info('# Parsed info keys: {0}'.format(list(ikeys.keys())))

    # Zlevels?
    zlevels = None
    user_z = False
    if 'keep_zlevels' in cp['I/O']:
        zlevels = cp['I/O'].as_list('keep_zlevels')
        zlevels = [int(z) for z in zlevels]
        user_z = True

    # Read predictand file
    pfile = cp['I/O']['predictand_file']
    log.info('# Reading predictand file: {0}'.format(pfile))
    varcont, dfinfo = dgio.read_predictand_file(pfile, vkeys=vkeys,
                                                ikeys=ikeys, zlevels=zlevels)
    timep = [varcont.time[0], varcont.time[-1]]
    # Log
    log.info('Predictands time period: {0} -> {1}'.format(timep[0],
                                                          timep[-1]))
    log.info('Number of altitude levels: {0} '.format(len(varcont.zlevels)))
    log.info('Number of variables: {0} '.format(len(varcont.varnames)))

    # Instanciate predictor object
    pfile = cp['I/O']['predictor_file']
    lag = cp['I/O'].as_int('lag_pred')

    pc = None
    if 'principle_components' in cp['I/O']:
        pc = cp['I/O'].as_float('principle_components')
        if pc < 0.: pc = None
    log.info('# Reading predictor file: {0}'.format(pfile))
    dfpred = dgio.read_predictor_file(pfile, lag=lag, pc=pc, normalize_period=timep)
    log.info('Predictors time period: {0} -> {1}'.format(dfpred.index[0],
                                                         dfpred.index[-1]))
    log.info('Number of predictors: {0} '.format(len(dfpred.keys())))

    # Cross validation iterator
    outercv = cp['Screen']['outercv']

    log.info('# Done initialization.')


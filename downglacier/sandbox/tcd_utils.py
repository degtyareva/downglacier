"""Helper routines for the analyses of the TDC paper.

Copyright: Fabien Maussion, 2014-2015

License: GPLv3+
"""

import pandas as pd
import numpy as np
from scipy.ndimage.measurements import label as label_region
from scipy.ndimage.measurements import find_objects
from datetime import datetime as dt
import shutil
import downglacier as dg
import downglacier.main as main
import os
from configobj import ConfigObj

def get_workingdir():
    """Cross-platorm default run directory."""

    from os.path import expanduser
    dir = os.path.join(expanduser("~"), 'DownGlacier_tcd_runs')
    if not os.path.exists(dir):
        os.makedirs(dir)

    return dir

def get_plot_dir():

    dir = os.path.join(get_workingdir(), 'plots')
    if not os.path.exists(dir):
        os.makedirs(dir)
    return dir

def run_standard(run=False):
    """Runs the standard config if asked and returns the path to the directory.

    It deletes the existing directory before run, be careful."""

    dir = os.path.join(get_workingdir(), 'run_standard')
    if run:
        if os.path.exists(dir):
            shutil.rmtree(dir)
        os.makedirs(dir)


        fnames = ['standard_run_predictands_shallap.nc',
                  'standard_run_predictors_era.nc',
                  'standard_run.cfg']
        for fn in fnames:
            shutil.copyfile(os.path.join(dg.file_dir, 'tcd_paper', fn),
                            os.path.join(dir, fn))

        pd = os.getcwd()
        os.chdir(dir)
        main.workflow('standard_run.cfg')
        os.chdir(pd)

    return dir


def run_checkinput_allz(run=False):
    """Runs the checkinput for all z if asked and returns the path to the
    directory.

    It deletes the existing directory before run, be careful."""

    dir = os.path.join(get_workingdir(), 'checkinput_allz')
    if run:
        if os.path.exists(dir):
            shutil.rmtree(dir)
        os.makedirs(dir)


        fnames = ['standard_run_predictands_shallap.nc',
                  'standard_run_predictors_era.nc']
        for fn in fnames:
            shutil.copyfile(os.path.join(dg.file_dir, 'tcd_paper', fn),
                            os.path.join(dir, fn))

        cp = ConfigObj(os.path.join(dg.file_dir, 'tcd_paper', 'standard_run.cfg'), file_error=True)
        if 'keep_zlevels' in cp['I/O']:
            del cp['I/O']['keep_zlevels']
        cp['Screen']['run'] = False
        cp['Regstats']['run'] = False
        cp['Diagnostic']['run'] = False
        cp.filename = os.path.join(dir, 'checkinput_allz.cfg')
        cp.write()

        pd = os.getcwd()
        os.chdir(dir)
        main.workflow('checkinput_allz.cfg')
        os.chdir(pd)

    return dir


def run_standard_allz(run=False):
    """Runs the standard config on all levels if asked and
    returns the path to the directory.

    It deletes the existing directory before run, be careful."""

    dir = os.path.join(get_workingdir(), 'run_standard_allz')
    if run:
        if os.path.exists(dir):
            shutil.rmtree(dir)
        os.makedirs(dir)


        fnames = ['standard_run_predictands_shallap.nc',
                  'standard_run_predictors_era.nc']
        for fn in fnames:
            shutil.copyfile(os.path.join(dg.file_dir, 'tcd_paper', fn),
                            os.path.join(dir, fn))

        cp = ConfigObj(os.path.join(dg.file_dir, 'tcd_paper', 'standard_run.cfg'), file_error=True)
        if 'keep_zlevels' in cp['I/O']:
            del cp['I/O']['keep_zlevels']

        cp.filename = os.path.join(dir, 'standard_run_allz.cfg')
        cp.write()

        pd = os.getcwd()
        os.chdir(dir)
        main.workflow('standard_run_allz.cfg')
        os.chdir(pd)

    return dir


def _read_cfg(exp):
    ifile = os.path.join(dg.file_dir, 'tcd_paper', 'standard_run.cfg')
    cp = ConfigObj(ifile, file_error=True)
    cp['I/O']['working_dir'] = './' + exp
    cp['CheckInput']['run'] = False
    cp['Regstats']['plots'] = False
    cp['Diagnostic']['plots'] = False
    return cp


def _sensi_method(dir):
    """Methods"""

    lisexps = []
    fnames = ['standard_run_predictands_shallap.nc',
              'standard_run_predictors_era.nc']
    for fn in fnames:
        shutil.copyfile(os.path.join(dg.file_dir, 'tcd_paper', fn),
                        os.path.join(dir, fn))

    exp = 'ref'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'step_pcor01'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['Screen']['algo'] = 'stepwise_regression_pcor'
    cp['Screen']['Keywords'] = {'threshold':0.01, 'maxpred':-1}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'step_pcor05'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['Screen']['algo'] = 'stepwise_regression_pcor'
    cp['Screen']['Keywords'] = {'threshold':0.05, 'maxpred':-1}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'step_rmse_l1o'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['Screen']['algo'] = 'stepwise_regression_cvrmse'
    cp['Screen']['Keywords'] = {'threshold':0.01, 'maxpred':-1,
                                'selectorcv':'w2'}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'step_rmse01'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['Screen']['algo'] = 'stepwise_regression_cvrmse'
    cp['Screen']['Keywords'] = {'threshold':0.01, 'maxpred':-1,
                                'selectorcv':'k4'}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'step_rmse05'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['Screen']['algo'] = 'stepwise_regression_cvrmse'
    cp['Screen']['Keywords'] = {'threshold':0.05, 'maxpred':-1,
                                'selectorcv':'k4'}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'step_pc_pcor01'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['I/O']['principle_components'] = 0.98
    cp['Screen']['algo'] = 'stepwise_regression_pcor'
    cp['Screen']['Keywords'] = {'threshold':0.01, 'maxpred':-1}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'step_pc_pcor05'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['I/O']['principle_components'] = 0.98
    cp['Screen']['algo'] = 'stepwise_regression_pcor'
    cp['Screen']['Keywords'] = {'threshold':0.05, 'maxpred':-1}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'lasso_8fold'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['Screen']['Keywords'] = {'selectorcv':'k8'}
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    exp = 'lasso_pc'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp['I/O']['principle_components'] = 0.98
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    pd = os.getcwd()
    os.chdir(dir)
    for l in lisexps:
        main.workflow(l + '.cfg')
    os.chdir(pd)

def _sensi_preds(dir):
    """Methods"""

    lisexps = []
    fnames = ['standard_run_predictands_shallap.nc',
              'standard_run_predictors_era.nc',
              'sensi_preds_hpa.nc',
              'sensi_preds_norh.nc',
              'sensi_preds_noscf.nc',
              'sensi_preds_notemp.nc',
              'sensi_preds_notempnorh.nc',
              'sensi_preds_vp.nc'
              ]

    for i, fn in enumerate(fnames):
        shutil.copyfile(os.path.join(dg.file_dir, 'tcd_paper', fn),
                        os.path.join(dir, fn))
        if i==0: continue
        if i==1:
            exp = 'preds_lag1'
        else:
            exp = fn.replace('sensi_', '').replace('.nc', '')
        cp = _read_cfg(exp)
        ofile = os.path.join(dir, exp + '.cfg')
        cp['I/O']['predictor_file'] = './' + fn
        if i==1:
            cp['I/O']['lag_pred'] = 1
        cp.filename = os.path.join(dir, exp, ofile)
        cp.write()
        lisexps.append(exp)

    exp = 'preds_ref'
    ofile = os.path.join(dir, exp + '.cfg')
    cp = _read_cfg(exp)
    cp.filename = os.path.join(dir, exp, ofile)
    cp.write()
    lisexps.append(exp)

    pd = os.getcwd()
    os.chdir(dir)
    for l in lisexps:
        main.workflow(l + '.cfg')
    os.chdir(pd)

def _sensi_reas(dir):
    """Methods"""

    lisexps = []
    fnames = ['standard_run_predictands_shallap.nc',
              'sensi_rea_cfsr.nc',
              'sensi_rea_era.nc',
              'sensi_rea_merra.nc',
              'sensi_rea_ncep.nc'
              ]

    for i, fn in enumerate(fnames):
        shutil.copyfile(os.path.join(dg.file_dir, 'tcd_paper', fn),
                        os.path.join(dir, fn))
        if i==0: continue

        exp = fn.replace('sensi_', '').replace('.nc', '')
        cp = _read_cfg(exp)
        ofile = os.path.join(dir, exp + '.cfg')
        cp['I/O']['predictor_file'] = './' + fn
        cp.filename = os.path.join(dir, exp, ofile)
        cp.write()
        lisexps.append(exp)

    pd = os.getcwd()
    os.chdir(dir)
    for l in lisexps:
        main.workflow(l + '.cfg')
    os.chdir(pd)


def _sensi_crossval(dir):
    """Methods"""

    lisexps = []
    fnames = ['standard_run_predictands_shallap.nc',
              'standard_run_predictors_era.nc']
    for fn in fnames:
        shutil.copyfile(os.path.join(dg.file_dir, 'tcd_paper', fn),
                        os.path.join(dir, fn))

    exps = ['w0', 'w1', 'w2', 'w4', 'k4', 'k2']
    for exp in exps:
        ofile = os.path.join(dir, exp + '.cfg')
        cp = _read_cfg(exp)
        cp['Screen']['outercv'] = exp
        cp.filename = os.path.join(dir, exp, ofile)
        cp.write()
        lisexps.append(exp)

    pd = os.getcwd()
    os.chdir(dir)
    for l in lisexps:
        main.workflow(l + '.cfg')
    os.chdir(pd)


def run_sensis(run=False):
    """Runs the sensitivity analysis if asked and
    returns the path to the directory.

    It deletes the existing directory before run, be careful."""

    dir = os.path.join(get_workingdir(), 'run_sensis')
    if run:
        if os.path.exists(dir):
            shutil.rmtree(dir)
        os.makedirs(dir)

        sdirs = ['method', 'preds', 'reas', 'crossval']
        for d in sdirs:
            os.makedirs(os.path.join(dir, d))

        # _sensi_method(os.path.join(dir, 'method'))
        # _sensi_preds(os.path.join(dir, 'preds'))
        # _sensi_reas(os.path.join(dir, 'reas'))
        _sensi_crossval(os.path.join(dir, 'crossval'))

    return dir


def get_all_sst_df(lag=0, longest=False, window=5):
    """Reads the SST and MEI index data files.

    Parameters
    ----------
    lag: number of months to "add" to the SST date
    longest: default is to return data from Oct 1979 (after lag) onwards
             set to true to start from Oct 1950 (after lag) onwards
    window: rolling mean window size



    SST data obtained from:
    http://www.cpc.ncep.noaa.gov/data/indices/ersst4.nino.mth.81-10.ascii

    MEI data obtained from:
    http://www.esrl.noaa.gov/psd/enso/mei/table.html
    (modified on a spreadsheet program to make it easier to parse)
    """

    if lag < 0:
        raise ValueError('lag should be positive')

    path = os.path.join(dg.file_dir, 'tcd_paper',
                        'index_ersst4.nino.mth.81-10.ascii')

    df = pd.read_csv(path, sep=' +', engine='python')

    time = pd.date_range(pd.datetime(1950, 1+lag, 1),
                         periods=len(df.index),
                         freq='MS')

    df = df[['ANOM', 'ANOM.3']]
    df.columns = ['NINO1.2', 'NINO3.4']

    dfm = df.apply(pd.rolling_mean, window=window, center=True)
    df = dfm.set_index(time)

    # MEI
    path = os.path.join(dg.file_dir, 'tcd_paper', 'index_mei.csv')
    mei_ts = pd.read_csv(path).iloc[:,1:].values.flatten()
    mei_time = pd.date_range(pd.datetime(1950, 1+lag, 1),
                             periods=len(mei_ts),
                             freq='MS')
    mei_df = pd.Series(mei_ts, index=mei_time)
    dfm = pd.rolling_mean(mei_df, window=window, center=True)
    mei_df = dfm

    y0 = 1950 if longest else 1979

    df = df.loc[dt(y0, 10, 1):dt(2013, 9, 1)]
    df['MEI'] = mei_df.loc[dt(y0, 10, 1):dt(2013, 9, 1)]

    ny, _ = divmod(len(df.index), 12)
    df['mi'] = np.tile(np.arange(1,13,1), ny)
    df['yi'] = np.repeat(np.arange(ny)+y0+1, 12)

    return df

def get_enso_df(lag=0, longest=False, window=5):
    """Reads Nino34 SST and classify nino nina periods according to
    Trenberth, 1997."""

    df = get_all_sst_df(lag=lag, longest=longest, window=window)
    df = df[['yi', 'mi', 'NINO3.4']].copy()
    df.columns = ['yi', 'mi', 'sst']

    thres = 0.4
    r, nr = label_region(df['sst'] > thres)
    nr = [i+1 for i, o in enumerate(find_objects(r)) if (len(r[o]) >= 6)]
    is_nino = [ri in nr for ri in r]
    r, nr = label_region(df['sst'] < (-thres))
    nr = [i+1 for i, o in enumerate(find_objects(r)) if (len(r[o]) >= 6)]
    is_nina = [ri in nr for ri in r]

    df['is_nina'] = is_nina
    df['is_nino'] = is_nino
    df['is_neutral'] = ~(df['is_nina'] | df['is_nino'])
    return df


def get_sst_baseplot(lag=0, longest=False, window=5):
    """Returns a dict with a couple of needed stuff for SST plotting"""

    df = get_enso_df(lag=lag, longest=longest, window=window)

    # Because BrokenBarHCollection.span_where is not working properly
    # we have to do A LOT of stuffs by ourselves
    if longest:
        majorTickNames = np.arange(13)*5 + 1954
    else:
        majorTickNames = np.arange(6)*5 + 1984

    majorTickPos = np.array([])
    for y in majorTickNames:
        majorTickPos = np.append(majorTickPos, np.where((df.index.month == 1) &
                                                        (df.index.year == y))[0])

    if longest:
        minorTickNames = np.arange(1950, 2014, 1)
    else:
        minorTickNames = np.arange(1980, 2014, 1)
    minorTickPos = np.array([])
    for y in minorTickNames:
        minorTickPos = np.append(minorTickPos, np.where((df.index.month == 1) &
                                                        (df.index.year == y))[0])

    nx = len(df['sst'])
    xtime = np.arange(nx)
    tspans = np.linspace(0, nx, nx*2+1)
    isnino = np.interp(tspans, xtime, df['is_nino'].values.astype(float)) != 0
    isnina = np.interp(tspans, xtime, df['is_nina'].values.astype(float)) != 0

    return {'nx': nx,
            'xtime': xtime,
            'isnino': isnino,
            'isnina': isnina,
            'tspans': tspans,
            'majorTickNames': majorTickNames,
            'majorTickPos': majorTickPos,
            'minorTickNames': minorTickNames,
            'minorTickPos': minorTickPos
            }

def welch_t_test(mu1, s1, N1, mu2, s2, N2):
    """http://en.wikipedia.org/wiki/Welch%27s_t_test"""

    import scipy.stats as stats

    # Construct arrays to make calculations more succint.
    N_i = np.array([N1, N2])
    dof_i = N_i - 1
    v_i = np.array([s1, s2]) ** 2
    # Calculate t-stat, degrees of freedom, use scipy to find p-value.
    t = (mu1 - mu2) / np.sqrt(np.sum(v_i / N_i))
    dof = (np.sum(v_i / N_i) ** 2) / np.sum((v_i ** 2) / ((N_i ** 2) * dof_i))
    p = stats.distributions.t.sf(np.abs(t), dof) * 2
    return t, p

def run_all_tcd():
    """Runs all experiments. Coud be a bit long"""

    # run_standard(run=True)
    # run_checkinput_allz(run=True)
    # run_standard_allz(run=True)
    run_sensis(run=True)


def run_all_figs(ipynb_dir):
    """Runs all fig notebooks in a directory. Requires installing runipy """

    from runipy.notebook_runner import NotebookRunner
    from IPython.nbformat.current import read
    import glob

    for f in glob.glob(os.path.join(ipynb_dir, 'Fig*.ipynb')):
        notebook = read(open(f), 'json')
        r = NotebookRunner(notebook)
        r.run_notebook()

if __name__ == "__main__":
    # This script will run all experiments and make all plots. It will
    # create a DownGlacier_tcd_runs directory in the user's home and might take
    # about an hour or more to complete (highly dependant on the number or
    # processors, etc)

    import timeit
    tic=timeit.default_timer()
    run_all_tcd()
    # run_all_figs('/home/mowglie/Documents/git/downglacier/examples/tcd_paper')
    toc=timeit.default_timer()
    print('Took {:.2f} minutes'.format((toc - tic)/60.))
README for the ENSO index files found in this directory

index_ersst4.nino.mth.81-10.ascii
---------------------------------

Monthly ERSSTv4 (1981-2010 base period) multi-index.

Downloaded from http://www.cpc.ncep.noaa.gov/data/indices on 15.07.2015

link:
http://www.cpc.ncep.noaa.gov/data/indices/ersst4.nino.mth.81-10.ascii



index_mei.csv
-------------

Multivariate ENSO Index (MEI) downloaded  on 15.07.2015 from
http://www.esrl.noaa.gov/psd/enso/mei/

Original link: http://www.esrl.noaa.gov/psd/enso/mei/table.html

Modified on a spreadsheet program to make it easier to parse.








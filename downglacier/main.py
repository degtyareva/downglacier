"""Main program.

Copyright: Fabien Maussion, 2014-2015

License: GPLv3+
"""

import os
import logging as log
import multiprocessing as mp
import pandas as pd
import time
import pickle
import copy
import argparse

# Locals
import downglacier.conf as conf
import downglacier.dgio as dgio
import downglacier.stats as stats
import downglacier.graphics as graphics


def checkinput():
    """Runs some tests on input data.

    Predictors are checked for collinarity, preditands
    are checked for autocorrelation, and it is checked
    if the surface energy balance is well understood."""

    log.info('--- Preprocess ---')

    todir = os.path.join(conf.wdir, 'checkinput')
    if not os.path.exists(todir):
        os.makedirs(todir)

    # Check for collinearity in predictors
    log.info('# Checking for collinearity in predictors')
    ofile = os.path.join(todir, 'collinearity_warnings.txt')
    f = open(ofile, 'w')
    res = stats.find_collinearity(conf.dfpred)
    for key, co in sorted(res.items()):
        r2 = key.split('_')
        t = 'r2 from ' + r2[0] + ' to ' + r2[1] + ':'
        log.info(t)
        f.write(t + '\n')
        if co is None:
            t = '  no colinear predictors'
            log.info(t)
            f.write(t + '\n')
            continue
        cnt = 0
        for v1, v2 in co:
            t = '  {} ({}) <> {} ({})'.format(v1, conf.dfpred[v1].long_name,
                                              v2, conf.dfpred[v2].long_name)
            f.write(t + '\n')
            cnt += 1
        log.warning('  found {} colinear predictors!'.format(cnt))
    f.close()

    # Check for auto-correlation in predictands
    log.info('# Writing out some statistics about the predictands')
    ofile = os.path.join(todir, 'predictands_stats.csv')
    res = stats.input_statistics(conf.varcont.dfdata())
    res.to_csv(ofile, float_format='%.2f')

    # See if we can reproduce the MB on a monthly basis using a
    # perfect downscaling approach
    myvarcont = copy.deepcopy(conf.varcont)
    for vn, v in myvarcont.items():
        preds = pd.DataFrame(v.ref_ts)
        modsel = stats.DummyModelSelector(v, preds)
        modsel.compute_fullmodel()
        v.model = modsel
        v.predict(preds)
    dgio.compute_diagnostics(myvarcont, cv=conf.outercv,
                                user_z=conf.user_z, dfinfo=conf.dfinfo)
    # output
    from matplotlib.backends.backend_pdf import PdfPages
    df = []
    with PdfPages(os.path.join(todir, 'check-smb.pdf')) as pdf:
        for vn, v in sorted(myvarcont.items()):
            if not v.isdiag: continue
            sdf = v.skillscores()
            sdf['key'] = vn
            df.append(sdf)
            if 'mbtotfromsum' not in vn: continue
            graphics.glacier_ts(v, ofile=pdf)
    df = pd.DataFrame(df, columns=list(df[0].keys()))
    df = df.set_index('key').sort_index()
    path = os.path.join(todir, 'precheck-results.csv')
    df.to_csv(path, float_format='%.2f')
    log.info('# Done preprocess.')


def screenvar(modsel, vn, i, n):
    """ Screen one variable.

    This function is meant to be sent on a single processor.

    Parameters
    ----------
    modsel: a ModelSelector instance
    vn: the variable name (for logging purposes)
    i: the index in array (for logging purposes)
    n: the length of the array (for logging purposes)

    Returns
    -------
    The same ModelSelector instance, screened
    """

    log.info('Screening var {}...'.format(vn))

    modsel.compute_fullmodel()

    if conf.cp['Screen'].as_bool('compute_cv'):
        modsel.compute_cv()

    if conf.cp['Screen'].as_bool('compute_outercv'):
        modsel.compute_outercv()

    log.info('... screened var {} ({}/{}).'.format(vn, i, n))
    return modsel

def screenvar_star(x):
    """Helper routine for multi-processing with python 2.7."""
    return screenvar(*x)

def screen():
    """Find the best model for all variables and write out the results."""

    log.info('--- Screen ---')

    # set-up
    preds = conf.dfpred.loc[conf.varcont.time]

    cv = conf.outercv
    algo = conf.cp['Screen']['algo'].lower()

    # Keywords
    def isfloat(value):
        """Checks if a string can be converted to a float"""
        try:
            float(value)
            return True
        except ValueError:
            return False

    keywds = dict()
    for key, val in conf.cp['Screen']['Keywords'].items():
        if val.isdigit():
            keywds[key] = int(val)
        elif isfloat(val):
            keywds[key] = float(val)
        else:
            keywds[key] = val

    # Associate a ModelSelector class to each option
    keymap = {'dummy': stats.DummyModelSelector,
              'stepwise_regression_pcor': stats.PartialCorModelSelector,
              'stepwise_regression_cvrmse': stats.CrossvalModelSelector,
              'lasso': stats.LassoModelSelector,
              'relaxed_lasso': stats.RelaxedLassoModelSelector,
              'constrained_lasso': stats.ConstrainedLassoModelSelector,
              'lasso_ols': stats.OLSLassoModelSelector
              }

    if algo in keymap:
        modeltype = keymap[algo]
    else:
        raise NotImplementedError('algo: {}'.format(algo))

    log.info('# Starting screening method: ' + algo)
    start_time = time.time()
    log.info('Keywords: {}'.format(keywds))

    nvars = len(conf.varcont)

    if conf.cp['I/O'].as_bool('multiprocessing'):
        # Compute all variables in parallel

        # Instanciate a ModelSelector for each variable
        poolargs = [(modeltype(v, preds, outercv=cv, **keywds), vn, i+1, nvars,)
                    for i, (vn, v) in enumerate(conf.varcont.items())]

        # Run
        nproc = conf.cp['I/O'].as_int('processes')
        if nproc == -1:
            nproc = None
        pool = mp.Pool(nproc)

        log.info('Parallel run with {} processes'.format(pool._processes))

        # Python 2.7 has no pool.starmap() method, for some reason
        poolout = pool.map(screenvar_star, poolargs, chunksize=1)
        # Unpack the results
        for i, v in enumerate(conf.varcont.values()):
            v.model = poolout[i]

    else:
        # Loop over the variables, run and store
        log.info('Serial run:')
        for i, (vn, v) in enumerate(conf.varcont.items()):
            modsel = modeltype(v, preds, outercv=cv, **keywds)
            v.model = screenvar(modsel, vn, i+1, nvars)

    # Logging
    mi, sec = divmod(time.time() - start_time, 60)
    hr, mi = divmod(mi, 60)
    log.info('Done screening: ' + algo
             + ' -- Keywords: {}'.format(keywds))
    log.info(
        'Needed {} hours {} minutes {} seconds.'.format(int(hr), int(mi),
                                                        int(sec)))

    log.info('# Compute predictions.')
    for vn, v in conf.varcont.items():
        v.predict(conf.dfpred)

    log.info('# Writing results.')

    todir = os.path.join(conf.wdir, 'screening')
    if not os.path.exists(todir):
        os.makedirs(todir)

    # CSV
    df = []
    for vn, v in conf.varcont.items():
        sdf = v.skillscores()
        sdf['key'] = vn
        df.append(sdf)
    df = pd.DataFrame(df, columns=list(df[0].keys()))
    df = df.set_index('key').sort_index()
    path = os.path.join(todir, 'results.csv')
    df.to_csv(path, float_format='%.2f')

    # Timeseries
    types = ['ref_ts', 'fullmodel_ts', 'cv_ts', 'outercv_ts', 'predict_ts']
    for type in types:
        df = conf.varcont.dfdata(vartype=type)
        path = os.path.join(todir, type + '.csv')
        df.to_csv(path)

    # Pickle
    path = os.path.join(todir, 'results.p')
    output = open(path, 'wb')
    pickle.dump(conf.varcont, output)
    output.close()

    # Netcdf

    log.info('# Done screening.')

def regstats():

    log.info('--- Regression statistics ---')

    # set-up
    todir = os.path.join(conf.wdir, 'screening')

    # This is a shortcut: the screen must or must not
    # have happened in this specific call but in all cases
    # the pickle should always be there
    ifile = os.path.join(todir, 'results.p')
    with open(ifile, 'rb') as f:
        conf.varcont = pickle.load(f)

    # Some output statistics
    from matplotlib.backends.backend_pdf import PdfPages
    with PdfPages(os.path.join(todir, 'regression-plots.pdf')) as pdf, \
            open(os.path.join(todir, 'regression-summary.txt'), 'wb') as tfile:
        for vn, v in sorted(conf.varcont.items()):
            if v.isdiag: continue
            linmo = v.model.fullmodel
            if isinstance(linmo, stats.OLSModel):
                # This is OLS.
                ols = linmo.model
                selp, _ = v.model.selpreds_summary()
                ss = ols.summary(yname=vn, xname=['c'] + selp.tolist(),
                                         title='### Results: ' + vn)
                tfile.write(ss.as_text().encode('UTF-8'))
                tfile.write('\n\n\n'.encode('UTF-8'))

                if conf.cp['Regstats'].as_bool('plots'):
                    graphics.regplots(ols, varname=vn, file=pdf)
            else:
                # This is Lasso.
                if conf.cp['Regstats'].as_bool('plots'):
                    graphics.lassopath(v.model, varname=vn, file=pdf)

    log.info('# Done regression statistics.')


def diagnosticplots():
    """Validation plots"""

    todir = os.path.join(conf.wdir, 'diagnostics')
    if not os.path.exists(todir): os.makedirs(todir)

    from matplotlib.backends.backend_pdf import PdfPages

    with PdfPages(os.path.join(todir, 'timeseries-plots.pdf')) as pdfs, \
            PdfPages(os.path.join(todir, 'regression-plots.pdf')) as pdfr:
        for vn, v in sorted(conf.varcont.items()):
            graphics.glacier_ts(v, pdfs)
            types = ['outercv_ts', 'cv_ts', 'fullmodel_ts']
            for typ in types:
                if getattr(v, typ) is not None:
                    graphics.scattervar(v.ref_ts, getattr(v, typ), v.ref_ts.index.date,
                                      varname=vn, vartype=typ, file=pdfr)
                    break


def diagnostic():
    """Compute the diagnostic variables and write out."""

    log.info('--- Diagnostic ---')

    # set-up
    indir = os.path.join(conf.wdir, 'screening')
    todir = os.path.join(conf.wdir, 'diagnostics')
    if not os.path.exists(todir):
        os.makedirs(todir)

    # This is a dirty shortcut: the screen must or must not
    # have happened in this specific call but in all cases
    # the pickle should always be there
    ifile = os.path.join(indir, 'results.p')
    with open(ifile, 'rb') as f:
        conf.varcont = pickle.load(f)


    dgio.compute_diagnostics(conf.varcont, cv=conf.outercv,
                                user_z=conf.user_z, dfinfo=conf.dfinfo)

    # CSV
    df = []
    for vn, v in conf.varcont.items():
        if not v.isdiag: continue
        sdf = v.skillscores()
        sdf['key'] = vn
        df.append(sdf)
    df = pd.DataFrame(df, columns=list(df[0].keys()))
    df = df.set_index('key').sort_index()
    path = os.path.join(todir, 'results.csv')
    df.to_csv(path, float_format='%.2f')

    # Predict CSV
    df = []
    for vn, v in conf.varcont.items():
        sdf = v.info()
        sdf['key'] = vn
        df.append(sdf)
    df = pd.DataFrame(df, columns=list(df[0].keys()))
    df = df.set_index('key').sort_index()
    path = os.path.join(todir, 'predict_info.csv')
    df.to_csv(path)

    # Timeseries
    types = ['ref_ts', 'fullmodel_ts', 'cv_ts', 'outercv_ts', 'predict_ts']
    for type in types:
        df = conf.varcont.dfdata(vartype=type)
        path = os.path.join(todir, type + '.csv')
        df.to_csv(path)

    # Pickle
    path = os.path.join(todir, 'results.p')
    output = open(path, 'wb')
    pickle.dump(conf.varcont, output)
    output.close()

    # Plots
    if conf.cp['Diagnostic'].as_bool('plots'):
        diagnosticplots()

    log.info('# Done Diagnostic.')

def workflow(cfgfile):
    """Run the DownGlacier program.

    Parameters
    ----------
    cfgfile: the path to the configuration file

    Side effects
    ------------
    Output files will be written in the output directory
    """

    # set up globals
    conf.initialize(cfgfile)

    if conf.cp['CheckInput'].as_bool('run'):
        checkinput()

    if conf.cp['Screen'].as_bool('run'):
        screen()

    if conf.cp['Regstats'].as_bool('run'):
        regstats()

    if conf.cp['Diagnostic'].as_bool('run'):
        diagnostic()

    log.info('# Done main().')

def run_dir(directory):
    """Shortcut routine to run all .cfg files in a directory"""

    pd = os.getcwd()
    os.chdir(directory)

    import glob
    for f in glob.glob('*.cfg'):
        workflow(f)

    os.chdir(pd)

def main():
    """Entry point for the downglacier script."""

    # Defaults
    parser = argparse.ArgumentParser(description='Glaciological statistical downscaling tool.')
    parser.add_argument('cfgfile', type=str, nargs=1,
                       help='the configuration file', metavar='configfile.cfg')

    # Go
    args = parser.parse_args()
    workflow(args.cfgfile[0])


if __name__ == "__main__":
    pass
